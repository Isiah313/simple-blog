class Comment < ActiveRecord::Base
  belongs_to :post

  scope :comment_order, -> { where(created_at: 'DESC') }
end
