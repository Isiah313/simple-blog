# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([( name: 'Chicago' ), ( name: 'Copenhagen' )])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Post.create!(title: 'minima quasi',
		body: "qui in tempore culpa ad cumque sit dolor quia possimus cupiditate voluptatem suscipit et qui rem qui
		enim eius totam dicta beatae iure quisquam at architecto expedita eos quos eligendi
		eos occaecati incidunt sed teius natus ut non animi placeat quae voluptatem culpa qui sed aut eveniet qui
		delectus facilis quia quo placeat doloremque quo voluptate optio velit in quidem quo modi eum et
		sed dolorum fugit excepturi cupiditate id ex aperiam porro molestiae autem omnis eum
		ut placeat sit ut ut cupiditate et explicabo nihil est inventore quasi dolorum possimus eaque repudiandae"
	)

Post.create!(title: 'consequuntur quaerat corrupti',
		body: "dolor quas modi inventore doloremque\nqui dolor est corrupti blanditiis rerum quia\nqui eveniet sapiente quibusdam earum\nalias qui qui nemo\n \r\tquos maxime rem consequatur\nsequi qui quia enim corrupti eveniet repudiandae doloremque optio\nipsam nobis molestias soluta dicta rerum\neligendi quo voluptate voluptatum tempora qui est\nnisi voluptates corporis dolore minima cupiditate possimus fuga vel\n \r\tvitae dolores ipsa maiores blanditiis\nautem ut fugiat\nminus sit reiciendis nihil ipsa\naut rerum reprehenderit eligendi qui quod architecto"
	)

Post.create!(title: 'delectus architecto',
		body: "dolor optio quibusdam autem\nlaboriosam vitae voluptatem qui\naut dolore maxime ea vero eum ut cumque\n \r\tomnis voluptas perferendis earum\net laborum ut laboriosam nemo velit\nodit sed dolores\n \r\tfugiat laboriosam voluptatem aperiam aut ipsum sunt\nassumenda minima in\net tempore aut ipsam eos molestiae dolorem et\nsit suscipit sint quis nostrum harum voluptas repellendus\neius earum id unde"
	)

Post.create!(title: 'nostrum autem totam non',
		body: "dolorem voluptatem totam quia officia in sit ab\neius est aliquid qui iusto\niusto at expedita impedit iste molestias\ndoloremque commodi quas consectetur voluptatum nihil\nillo nesciunt facilis non dolores aperiam repellendus quia tenetur\n \r\tdelectus repellat ratione\naut neque earum\nexercitationem neque veniam aut\n \r\tomnis unde voluptas occaecati consequatur molestiae\ntempore ut et vel aliquid nemo aut corrupti omnis\nmagnam doloribus ea blanditiis\nsint aspernatur corporis"
	)

Post.create!(title: 'enim officia sint',
		body: "culpa omnis labore officiis\nquis modi hic illo magnam\nfugit totam laudantium eos ab velit qui\nrecusandae iusto aut nam\nlaborum illum minus\n \r\tlaudantium qui quia\nmolestias asperiores adipisci\nvelit ipsa suscipit\namet molestias nobis officiis nihil id atque\n \r\tdoloremque dolore quisquam\nvoluptate omnis earum rerum architecto\nfugiat amet in omnis neque eum nulla iusto\neum ullam quo laboriosam aspernatur"
	)

Post.create!(title: 'dignissimos eveniet non eos',
		body: "quos quam voluptatem ut eum ullam aut doloremque\nipsa asperiores est cumque minus qui ut vel\nrerum asperiores rem est voluptatem\nincidunt nihil quam qui et ut tempora omnis\nveritatis iusto eum adipisci beatae\n \r\tet voluptatem amet et pariatur\nillo libero beatae iusto repudiandae\nnihil enim et voluptate tempore asperiores\nillo necessitatibus iure vero ipsa omnis voluptas voluptatem\noccaecati voluptatibus optio adipisci aut aliquid\n \r\tdelectus aliquid est veritatis aperiam autem officiis cum\nqui perspiciatis aperiam quos nihil possimus adipisci natus culpa\nut et praesentium eveniet ratione\nquia impedit vero rem quae et quia aut molestiae\nanimi recusandae e"
	)

Post.create!(title: 'aut quo optio consequatur illum',
		body: "debitis quis ut\nimpedit blanditiis atque libero\nmagnam nobis quis dolor at ut eos cumque\nipsa facilis autem unde voluptatum nobis corrupti soluta quo\n \r\tut sunt qui\noccaecati saepe ipsam sed iure\ndebitis odio dolores harum\nexplicabo est distinctio exercitationem itaque\n \r\tfugiat debitis maxime dolorem earum quae et sunt aperiam\ndicta nobis dolores iusto\nipsum doloremque vitae ducimus praesentium est tenetur enim\neaque at quibusdam quia itaque accusantium qui"
	)

Post.create!(title: 'atque eaque fugiat et qui explicabo',
		body: "nam eos aut blanditiis\nenim est voluptatem molestiae\net molestiae enim vel labore voluptatem est\n \r\tlabore esse qui voluptate\nodit voluptate dolor quam\neum assumenda nulla nam enim\npossimus id est sunt\nsimilique inventore velit unde provident incidunt quam rerum magni\n \r\tconsequatur velit quae molestiae a cum rem sit labore\nipsa corrupti harum quia illo\nplaceat est id\nreprehenderit at et laborum officiis tempore a\nautem et enim ipsam"
	)

Post.create!(title: 'ut eligendi minima magnam commodi',
		body: "explicabo commodi ducimus ea consequatur voluptas omnis\nvoluptatem rerum et consequatur dolorem consequuntur voluptas\ndolorem sunt ut\nadipisci odit totam dolorem quidem accusantium pariatur beatae dolorum\n \r\tnesciunt vero consequatur nobis dolorem voluptatum ullam accusamus\net enim quia distinctio amet\nut velit aut laudantium harum earum minima\net qui animi\nsunt nulla veniam velit ut ea illo\n \r\tiure cumque molestiae\nmaxime qui natus corporis\ndolor aspernatur inventore doloremque molestiae ex omnis voluptate tempora\nex quod dolorem magn"
	)

Post.create!(title: 'eaque corrupti quasi omnis',
		body: "officia eius tempore\nrepellendus sed enim dicta fugiat\nquisquam ut sapiente architecto culpa\nid non ut sapiente doloremque\n \r\tnulla error consequatur pariatur provident atque vel\nrerum distinctio et accusantium culpa quos ducimus maiores pariatur\nminima in illum delectus sed sint unde facere fuga\nveniam qui excepturi cupiditate eaque vel consequatur impedit dolor\n \r\teveniet possimus earum quasi autem\neum et quos architecto magnam cupiditate consequuntur ratione incidunt\ncommodi aliquam asperiores eveniet unde dolor\nsequi nostrum consequatur voluptas\nquis non omnis assumenda et et vero aut"
	)



