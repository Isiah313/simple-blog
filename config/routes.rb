Rails.application.routes.draw do
  
  get :login, to: 'sessions#new'
  post :login, to: 'sessions#create'
  get :logout, to: 'sessions#destroy'

  resources :sessions

  resources :users

  resources :posts do
  	resources :comments
  end

  root 'posts#index'

  get '/about', to: 'pages#about'
end
